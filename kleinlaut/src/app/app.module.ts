import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatExpansionModule } from "@angular/material";
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { UICarouselModule } from "ui-carousel";
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import 'hammerjs';
import 'mousetrap';
import { ModalGalleryModule } from 'angular-modal-gallery';

import { AppComponent } from './app.component';
import { ProgrammComponent } from './programm/programm.component';
import { HomeComponent } from './home/home.component';
import { WirComponent } from './wir/wir.component';
import { FestivalComponent } from './festival/festival.component';
import { HelfenComponent } from './helfen/helfen.component';
import { InfiniteCarouselComponent } from './infinite-carousel/infinite-carousel.component';

const appRoutes: Routes = [
  { path: 'programm', component: ProgrammComponent},
  { path: 'wir', component: WirComponent},
  { path: 'festival', component: FestivalComponent},
  { path: 'helfen', component: HelfenComponent},
  { path: '', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ProgrammComponent,
    HomeComponent,
    WirComponent,
    FestivalComponent,
    HelfenComponent,
    InfiniteCarouselComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ModalGalleryModule.forRoot(),
    UICarouselModule,
    MatExpansionModule,
    NoopAnimationsModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { 

}
