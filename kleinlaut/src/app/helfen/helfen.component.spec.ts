import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelfenComponent } from './helfen.component';

describe('HelfenComponent', () => {
  let component: HelfenComponent;
  let fixture: ComponentFixture<HelfenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelfenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelfenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
