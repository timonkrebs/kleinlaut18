import { Component, OnInit } from '@angular/core';
import { Image, Description, DescriptionStrategy, PlainGalleryConfig, PlainGalleryStrategy, LineLayout} from 'angular-modal-gallery';


@Component({
  selector: 'app-festival',
  templateUrl: './festival.component.html',
  styleUrls: ['./festival.component.css']
})
export class FestivalComponent implements OnInit {

  public PlainGalleryRowConfig: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.ROW,
    layout: new LineLayout({ width: '100%', height: 'auto' }, { length: 3, wrap: true }, 'flex-start')
  };

  public customFullDescriptionHidden: Description = {
    strategy: DescriptionStrategy.ALWAYS_HIDDEN
  };
  
  public plan: Image[] = [
    new Image(0,{ img: 'https://www.kleinlautfestival.ch/img/Plan2018.jpg'})
  ];

  constructor() { }

  ngOnInit() {
  }

}
