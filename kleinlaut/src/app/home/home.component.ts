import { Component, OnInit } from '@angular/core';
import { InfiniteCarouselComponent } from '../infinite-carousel/infinite-carousel.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
